package com.atic.server;

/**
 * Defines the skeleton of a server.
 */
public interface Server {

  /**
   * Starts this server if not running.
   */
  void startServer() throws Exception;

  /**
   * Stops this server if running.
   */
  void stopServer();

  /**
   * Returns true if this server is running.
   */
  boolean isRunning();
}
