package com.atic.server;

/**
 * Responsible for inspecting a given hostname.
 */
public interface HostnameInspector {

  /**
   * Returns true if the given hostname should not be resolved.
   */
  boolean shouldBlackList(String hostname);
}
