package com.atic.server.resolver;

import static com.atic.server.DnsServer.createPacketForForwarding;

import com.atic.server.DnsQuery;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.time.Duration;

/**
 * Forwards DNS query to {@link #REMOTE_DNS_SERVER} in cleartext.
 */
public class CleartextResolver implements Resolver {

  private static final SocketAddress REMOTE_DNS_SERVER =
      new InetSocketAddress("8.8.8.8", 53);
  private static final int MAX_DNS_RESPONSE_SIZE_BYTES = 1024;

  private static final CleartextResolver instance = new CleartextResolver();

  private CleartextResolver() {
  }

  public static CleartextResolver get() {
    return instance;
  }

  @Override
  public DatagramPacket resolve(DnsQuery dnsQuery, Duration timeout) throws IOException {
    try (DatagramSocket forwardingSocket = new DatagramSocket()) {
      forwardingSocket.setSoTimeout((int) timeout.toMillis());
      // Send the query to remote server.
      DatagramPacket queryForRemoteServer = createQueryForRemoteServer(dnsQuery.query);
      forwardingSocket.send(queryForRemoteServer);

      return getResponse(forwardingSocket);
    }
  }

  private static DatagramPacket createQueryForRemoteServer(
      DatagramPacket requestFromLocalPeer) {
    return createPacketForForwarding(REMOTE_DNS_SERVER,
        requestFromLocalPeer);
  }

  private static DatagramPacket getResponse(DatagramSocket socket)
      throws IOException {
    byte[] rawResponse = new byte[MAX_DNS_RESPONSE_SIZE_BYTES];
    DatagramPacket dnsResponse = new DatagramPacket(rawResponse,
        rawResponse.length);
    socket.receive(dnsResponse);
    return dnsResponse;
  }
}
