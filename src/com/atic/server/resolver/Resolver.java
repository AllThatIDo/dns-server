package com.atic.server.resolver;

import com.atic.server.DnsQuery;
import java.io.IOException;
import java.net.DatagramPacket;
import java.time.Duration;

/**
 * Responsible for resolving a DNS query.
 */
public interface Resolver {

  /**
   * Returns DNS response for the given {@code dnsQuery} within the specified timeout.
   */
  DatagramPacket resolve(DnsQuery dnsQuery, Duration timeout) throws IOException;
}
