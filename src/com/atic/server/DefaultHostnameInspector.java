package com.atic.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * Prevents resolving host names that are part of 'hostnames_blacklist.txt'.
 */
public class DefaultHostnameInspector implements HostnameInspector {

  private static final String BLACKLIST_FILE = "com/atic/res/hostnames_blacklist.txt";

  private final Set<String> hostNamesBlacklist = new HashSet<>();

  public DefaultHostnameInspector() throws IOException {
    setupBlackList();
  }

  @Override
  public boolean shouldBlackList(String hostname) {
    return hostNamesBlacklist.contains(hostname);
  }

  private void setupBlackList() throws IOException {
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getClassLoader().getResourceAsStream(BLACKLIST_FILE)))) {
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        if (line.startsWith("#")) {
          continue;
        }
        hostNamesBlacklist.add(line);
      }
    }
  }
}
