package com.atic.server;

import java.net.DatagramPacket;

/**
 * Encapsulates properties of a DNS request packet.
 */
public final class DnsQuery {

  public final int id;
  public final String hostname;
  public final DatagramPacket query;

  public DnsQuery(int id, String hostname, DatagramPacket query) {
    this.id = id;
    this.hostname = hostname;
    this.query = query;
  }
}
