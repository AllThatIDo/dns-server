package com.atic.server;

import com.atic.server.resolver.CleartextResolver;
import com.atic.server.resolver.Resolver;
import com.atic.util.AverageMaintainer;
import com.atic.util.DnsQueryParser;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Duration;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles incoming DNS requests from peers over local subnet.
 *
 * <p>Upon receiving a request from local peer, it forwards it to appropriate {@link Resolver}, and
 * forwards corresponding response (from the remote server) back to the local peer.
 */
public class DnsServer implements Server {

  private static final String TAG = "DnsServer";

  private static final int DNS_SERVER_PORT = 53;
  private static final int MAX_DNS_QUERY_SIZE_BYTES = 512;

  private static final int THREAD_POOL_CORE_SIZE = 2;
  private static final int THREAD_POOL_MAX_SIZE = 6;
  private static final long THREAD_POOL_KEEP_ALIVE_TIME_MILLIS =
      Duration.ofMinutes(1).toMillis();

  private static final ByteOrder NETWORK_BYTE_ORDER = ByteOrder.BIG_ENDIAN;

  private final ThreadPoolExecutor threadPool =
      new ThreadPoolExecutor(
          THREAD_POOL_CORE_SIZE, THREAD_POOL_MAX_SIZE,
          THREAD_POOL_KEEP_ALIVE_TIME_MILLIS, TimeUnit.MILLISECONDS,
          new LinkedBlockingQueue<>());

  private final HostnameInspector hostnameInspector;

  private volatile boolean isRunning;

  public DnsServer(HostnameInspector hostnameInspector) {
    this.hostnameInspector = hostnameInspector;
  }

  /**
   * Starts this server and blocks calling thread for as long as this server is running.
   */
  @Override
  public synchronized void startServer() throws IOException {
    if (isRunning()) {
      return;
    }
    isRunning = true;
    try (DatagramSocket serverSocket = new DatagramSocket(DNS_SERVER_PORT)) {
      run(serverSocket);
    }
  }

  private void run(DatagramSocket serverSocket) throws IOException {
    Logger.getLogger(TAG).log(Level.INFO, "Started DNS server");
    while (isRunning) {
      byte[] rawQuery = new byte[MAX_DNS_QUERY_SIZE_BYTES];
      DatagramPacket dnsQuery =
          new DatagramPacket(rawQuery, MAX_DNS_QUERY_SIZE_BYTES);
      // Wait for DNS query from a local peer.
      serverSocket.receive(dnsQuery);
      threadPool.submit(new DnsQueryHandler(dnsQuery, serverSocket, hostnameInspector));
    }
  }

  @Override
  public synchronized void stopServer() {
    if (!isRunning()) {
      return;
    }
    isRunning = false;
    threadPool.shutdownNow();
  }

  @Override
  public boolean isRunning() {
    return isRunning;
  }

  /**
   * Creates a {@link DatagramPacket} from the given {@code srcPacket} and {@code destAddress} in
   * {@link #NETWORK_BYTE_ORDER}.
   */
  public static DatagramPacket createPacketForForwarding(
      SocketAddress destAddress, DatagramPacket srcPacket) {
    ByteBuffer rawSrcPacketData =
        ByteBuffer.wrap(srcPacket.getData(),
            srcPacket.getOffset(),
            srcPacket.getLength());
    rawSrcPacketData.order(NETWORK_BYTE_ORDER);
    byte[] srcPacketDataInNetworkOrder =
        new byte[srcPacket.getLength() - srcPacket.getOffset()];
    rawSrcPacketData.get(srcPacketDataInNetworkOrder);

    return new DatagramPacket(srcPacketDataInNetworkOrder,
        srcPacketDataInNetworkOrder.length, destAddress);
  }

  private static class DnsQueryHandler implements Runnable {

    private static final Duration RESPONSE_TIMEOUT = Duration.ofSeconds(3);

    private static final AtomicLong successfulRequests = new AtomicLong();
    private static final AtomicLong timedOutRequests = new AtomicLong();
    private static final AtomicLong failedRequests = new AtomicLong();
    private static final AtomicLong blackListedRequests = new AtomicLong();
    private static final AverageMaintainer averageDurationMillis = new AverageMaintainer();

    private final DatagramPacket dnsQueryFromLocalPeer;
    private final DatagramSocket serverSocket;
    private final HostnameInspector hostnameInspector;

    DnsQueryHandler(DatagramPacket dnsQueryFromLocalPeer,
        DatagramSocket serverSocket, HostnameInspector hostnameInspector) {
      this.dnsQueryFromLocalPeer = dnsQueryFromLocalPeer;
      this.serverSocket = serverSocket;
      this.hostnameInspector = hostnameInspector;
    }

    @Override
    public void run() {
      final long startMillis = System.currentTimeMillis();

      DnsQuery dnsQuery = DnsQueryParser.parse(dnsQueryFromLocalPeer);
      if (dnsQuery != null && hostnameInspector.shouldBlackList(dnsQuery.hostname)) {
        blackListedRequests.incrementAndGet();
        return;
      }

      try {
        DatagramPacket responseFromRemoteServer = CleartextResolver.get()
            .resolve(dnsQuery, RESPONSE_TIMEOUT);

        // Create response for the local peer.
        final SocketAddress localPeerAddress =
            new InetSocketAddress(dnsQueryFromLocalPeer.getAddress(),
                dnsQueryFromLocalPeer.getPort());
        DatagramPacket responseForPeer =
            createResponseForPeer(localPeerAddress,
                responseFromRemoteServer);

        // Send response back to local peer.
        serverSocket.send(responseForPeer);
        successfulRequests.incrementAndGet();
      } catch (SocketTimeoutException e) {
        timedOutRequests.incrementAndGet();
      } catch (IOException e) {
        Logger.getLogger(TAG).log(Level.SEVERE, "Failed to handle query", e);
        failedRequests.incrementAndGet();
      } finally {
        long durationMillis = System.currentTimeMillis() - startMillis;
        averageDurationMillis.add(durationMillis);
        DnsStatsLogger.maybeLogToStdOut();
      }
    }

    private static DatagramPacket createResponseForPeer(
        SocketAddress peer, DatagramPacket responseFromRemoteServer) {
      return createPacketForForwarding(peer, responseFromRemoteServer);
    }
  }

  private static final class DnsStatsLogger {

    private static final long LOGGING_INTERVAL_MILLIS = Duration.ofMinutes(5).toMillis();

    private static long lastLogTimestampMillis;

    private DnsStatsLogger() {
    }

    private static void maybeLogToStdOut() {
      if (!shouldLog()) {
        return;
      }
      lastLogTimestampMillis = System.currentTimeMillis();
      Logger.getLogger(TAG).log(Level.INFO, getLogMsg());
    }

    private static String getLogMsg() {
      return "Failed: " + DnsQueryHandler.failedRequests.get() +
          ", TimedOut: " + DnsQueryHandler.timedOutRequests.get() +
          ", BlackListed: " + DnsQueryHandler.blackListedRequests.get() +
          ", Successful: " + DnsQueryHandler.successfulRequests.get() +
          ", Average resolution(ms): " + DnsQueryHandler.averageDurationMillis.get();
    }

    private static boolean shouldLog() {
      if (lastLogTimestampMillis == 0) {
        return true;
      }
      final long elapsedTimeSinceLastLogMillis =
          System.currentTimeMillis() - lastLogTimestampMillis;
      return elapsedTimeSinceLastLogMillis >= LOGGING_INTERVAL_MILLIS;
    }
  }
}
