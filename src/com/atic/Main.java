package com.atic;

import com.atic.server.DefaultHostnameInspector;
import com.atic.server.DnsServer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

  private static final String TAG = "Main";

  public static void main(String[] args) {
    DnsServer dnsServer = null;
    try {
      dnsServer = new DnsServer(new DefaultHostnameInspector());
      dnsServer.startServer();
    } catch (IOException e) {
      Logger.getLogger(TAG).log(Level.SEVERE, "Failed to start DNS server", e);
      if (dnsServer != null) {
        dnsServer.stopServer();
      }
    }
  }
}
