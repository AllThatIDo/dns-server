package com.atic.util;

/**
 * Maintains average for long values.
 */
public final class AverageMaintainer {

  private long numValues;
  private long sum;

  /**
   * Adds given value towards updating current average.
   */
  public synchronized void add(long value) {
    numValues++;
    sum += value;
  }

  /**
   * Returns current average.
   */
  public synchronized double get() {
    if (numValues == 0) {
      return 0;
    }
    return sum / (double) numValues;
  }
}
