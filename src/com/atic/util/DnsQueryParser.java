package com.atic.util;

import com.atic.server.DnsQuery;
import com.sun.istack.internal.Nullable;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

/**
 * Utility class for parsing hostname from {@link DatagramPacket}.
 */
public final class DnsQueryParser {

  private DnsQueryParser() {
  }

  /**
   * Returns hostname from the given {@code dnsQueryPacket}.
   */
  @Nullable
  public static DnsQuery parse(DatagramPacket dnsQueryPacket) {
    // DNS packet format:
    // 2 bytes ID
    // 2 bytes Flags
    // 2 bytes Question count
    // 2 bytes Answer count
    // 2 bytes Authority RR count
    // 2 bytes Additional RR count
    // Queries, where each query starts with 1 byte representing length of name, followed by name.
    ByteBuffer queryInBigEndian = getRawQueryInBigEndian(dnsQueryPacket);

    int questionCount = parseQuestionCount(queryInBigEndian);
    if (questionCount == 0) {
      return null;
    }

    int id = parseId(queryInBigEndian);
    String hostname = parseNameFromFirstQuery(queryInBigEndian);
    return new DnsQuery(id, hostname, dnsQueryPacket);
  }

  private static int parseId(ByteBuffer queryInBigEndian) {
    // bytes 1 through 2 represents ID
    return ByteBuffer.wrap(queryInBigEndian.array(), /* offset= */ 0, /* length= */ 2).getShort();
  }

  private static int parseQuestionCount(ByteBuffer queryInBigEndian) {
    // bytes 5 through 6 represents question count
    return ByteBuffer.wrap(queryInBigEndian.array(), /* offset= */ 4, /* length= */ 2).getShort();
  }

  private static String parseNameFromFirstQuery(ByteBuffer queryInBigEndian) {
    // Starts at 13th byte position which represents length.
    // Say hostname is: 'abc.com', then here's how byte[] would look like (starting at 12th index):
    // '3', 'a', 'b', 'c', '3', 'c', 'o', 'm', '0'
    // This continues on until the length != 0.
    StringBuilder sb = new StringBuilder();

    int lengthIdx = 12;
    int domainNameLength = queryInBigEndian.array()[lengthIdx] & 0xFF;
    int domainNameStartIdx = lengthIdx + 1;
    while (true) {
      sb.append(new String(queryInBigEndian.array(), domainNameStartIdx, domainNameLength,
          StandardCharsets.UTF_8));
      lengthIdx = domainNameStartIdx + domainNameLength;
      domainNameLength = queryInBigEndian.array()[lengthIdx] & 0xFF;
      domainNameStartIdx = lengthIdx + 1;
      if (domainNameLength == 0) {
        break;
      }
      sb.append('.');
    }
    return sb.toString();
  }

  private static ByteBuffer getRawQueryInBigEndian(DatagramPacket query) {
    ByteBuffer rawQuery = ByteBuffer.wrap(query.getData(), query.getOffset(), query.getLength());
    rawQuery.order(ByteOrder.BIG_ENDIAN);
    return rawQuery;
  }
}
