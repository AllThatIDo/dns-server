# DNS server

A simple DNS proxy server in JAVA. It forwards all the incoming requests to Google DNS.

Update `res/hostnames_blacklist.txt` in case you want to blacklist certain hostnames.

To build, you can either use Intellij and import this project. 

To use `ant`, update `dns-server.properties` with `jdk-home` path.

TODO: Add support for DNS-over-TLS.
